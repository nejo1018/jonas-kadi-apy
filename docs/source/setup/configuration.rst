Configuration
=============

To connect an instance of Kadi4Mat to this library, the host of the Kadi4Mat instance as
well as a personal access token (PAT) are required. The host is the fully qualified
domain name of a Kadi4Mat instance, e.g. ``https://kadi4mat.example.edu``. PATs on the
other hand have to be created via the web interface of the same Kadi4Mat instance in the
menu found in *Settings > Access tokens*. Please also refer to the corresponding
`documentation <https://kadi4mat.readthedocs.io/en/stable/httpapi/general.html
#personal-access-tokens>`__ of Kadi4Mat itself.

You can either directly pass the token and host to the :class:`.KadiManager` class (see
also the :ref:`library <usage-library>` section) or use a config file to store this
information, **which is usually recommended**. Note that when using both ways in
parallel, the former takes precedence.

The kadi-apy CLI provides various convenience commands to create and update the config
file:

.. code-block:: shell

    kadi-apy config create   # Create a basic config file.
    kadi-apy config set-host # Change a host in the config file.
    kadi-apy config set-pat  # Change a PAT in the config file.

The first command will create a sample config file similar to the following (without the
comments), residing in a suitable location in your home directory, depending on the
operating system:

.. code-block:: ini

    ; Global settings.
    [global]
    ; The name of the default Kadi4Mat instance to use.
    default = my_instance
    ; Timeout in seconds for all requests.
    timeout = 60
    ; Whether to verify the SSL/TLS certificate of the host.
    verify = True
    ; Optional custom CA bundle to use for SSL/TLS verification.
    ca_bundle = /path/to/certfile
    ; A comma-separated whitelist of hosts that kadi-apy is allowed to redirect to. Note
    ; that the "Authorization" header will always be stripped on redirect.
    whitelist = https://example.com,https://example2.com

    ; Specific settings for the current default instance.
    [my_instance]
    ; Host of the instance.
    host = https://kadi.example.edu
    ; PAT corresponding to the instance.
    pat = <PAT>

    ; Specific settings for another instance.
    [my_second_instance]
    host = https://kadi.example2.edu
    pat = <PAT>
