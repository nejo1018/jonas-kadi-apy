Installation
============

The library can be installed using ``pip3`` (or simply ``pip``, if there is no dedicated
version of ``pip`` for Python 3 installed), which generally comes bundled with regular
Python installations. Note that a Python version **>=3.8** is required to install the
latest version of this library:

.. code-block:: shell

    pip3 install kadi-apy

If you want to use the CLI, make sure the ``kadi-apy`` command is runnable from
anywhere. Depending on the type of installation and if a virtual environment is used or
not, the ``PATH`` system variable **may** need to be adapted accordingly to include the
path the executable resides in. For example, the following command can be used on Linux,
assuming the executable resides in ``~/.local/bin``:

.. code-block:: shell

  export PATH=${HOME}/.local/bin:${PATH}

This line can also be added to ``.bashrc`` so it will be executed each time a new
terminal is opened:

.. code-block:: shell

  echo 'export PATH=${HOME}/.local/bin:${PATH}' >> ${HOME}/.bashrc

On Windows, the ``PATH`` variable can be modified permanently by editing the respective
environment variable in the advanced system settings of the control panel. The Python
installer usually can do this automatically, but the corresponding option has to be
selected explicitely.

Tab completion is also available for some shells in some capacity, namely ``bash``,
``zsh`` and ``fish``, and can be activated using:

.. code-block:: shell

    kadi-apy config activate-autocompletion
