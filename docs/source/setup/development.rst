Development
===========

When installing the library from source for development instead, it is recommended to
install it in editable mode, which simply creates a link to the sources so all changes
are reflected in the installed package immediately. The following command will also
install some additional development dependencies as defined in ``pyproject.toml``:

.. code-block:: shell

    pip3 install -e .[dev]

Especially for development, running the installation inside a virtual environment is
recommended, see `Virtualenv <https://virtualenv.pypa.io/en/latest/>`__ for more
information.

At this point, it is also recommended to already install the `pre-commit
<https://pre-commit.com>`__ hooks by running:

.. code-block:: shell

    pre-commit install

Besides pre-commit, `Pylint <https://www.pylint.org>`__ is used as a static code
analysis tool. It can be used to check the whole project by running:

.. code-block:: shell

    pylint kadi_apy

For contributions, it is recommended to first create a fork of this project and clone
it. The main repository can be added as an additional remote to the cloned project,
often called ``upstream``. Also checkout the ``development`` branch, which is currently
used as the main development branch.

Before creating a merge request on the main repository, make sure the GitLab CI runs
through successfully in your fork. If applicable, make sure to add an entry about any
new functionality to ``HISTORY.md``. You may also add yourself as a contributor to
``AUTHORS.md``.
