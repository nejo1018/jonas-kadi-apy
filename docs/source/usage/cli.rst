CLI
===

The command line interface (CLI) is an alternative way to use most of the functionality
that kadi-apy provides. It is especially useful for scripting and the integration with
programming languages other than Python. However, due to the nature of CLI tools, it is
less flexible in comparison with the Python library.

The central entry point to the CLI is provided by the main ``kadi-apy`` command:

.. code-block:: shell

    kadi-apy --help

.. click:: kadi_apy.cli.main:kadi_apy
   :prog: kadi-apy
   :nested: full
