CLI Library
===========

The Python CLI library is mainly useful to implement custom CLI commands. The provided
functionality is usually less flexible in comparison with the regular Python library,
but it also provides some built-in error handling and console output.

In general, its functionality can be used similarly to the regular Python library:

.. code-block:: python3

    from kadi_apy import CLIKadiManager

    manager = CLIKadiManager()

    record = manager.record(id=1)
    record.upload_file("/path/to/file.txt")

CLIKadiManager
--------------

.. autoclass:: kadi_apy.cli.core.CLIKadiManager
    :members:
    :inherited-members:

CLIRecord
---------

.. autoclass:: kadi_apy.cli.lib.records.CLIRecord
    :members:
    :inherited-members:

CLICollection
-------------

.. autoclass:: kadi_apy.cli.lib.collections.CLICollection
    :members:
    :inherited-members:

CLITemplate
-----------

.. autoclass:: kadi_apy.cli.lib.templates.CLITemplate
    :members:
    :inherited-members:

CLIGroup
--------

.. autoclass:: kadi_apy.cli.lib.groups.CLIGroup
    :members:
    :inherited-members:

CLIUser
-------

.. autoclass:: kadi_apy.cli.lib.users.CLIUser
    :members:
    :inherited-members:

CLIMiscellaneous
----------------

.. autoclass:: kadi_apy.cli.lib.misc.CLIMiscellaneous
    :members:
    :inherited-members:

CLISearch
---------

.. autoclass:: kadi_apy.cli.search.CLISearch
    :members:
    :inherited-members:

Decorators
----------

.. automodule:: kadi_apy.cli.decorators
    :members:
