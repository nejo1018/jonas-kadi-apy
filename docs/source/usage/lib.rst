.. _usage-library:

.. include:: ../snippets/kadi_docs.rst

Library
=======

The Python library is the most important and most flexible way to use the functionality
that kadi-apy provides. The central entry point for most functionality is the
:class:`.KadiManager` class. The following shows a basic example on how to use it:

.. code-block:: python3

    from kadi_apy import KadiManager

    manager = KadiManager()

    # Retrieve an existing record by its ID.
    record = manager.record(id=1)

    # Upload a file to the record.
    response = record.upload_file("/path/to/file.txt")

It is also possible to use the :class:`.KadiManager` class as a context manager. This
ensures that the underlying `session
<https://requests.readthedocs.io/en/latest/user/advanced/#session-objects>`__ is always
closed after exiting the ``with`` block:

.. code-block:: python3

    from kadi_apy import KadiManager

    with KadiManager() as manager:
        # Do something with the manager.

.. note::
    Most methods used to interact with resources return a response object as defined in
    the `requests <https://requests.readthedocs.io/en/latest>`__ Python library, which
    can be used to detect whether a request has been successful.

Next to records (:class:`.Record`), it is also possible to interact with other types of
resources, namely collections (:class:`.Collection`), templates (:class:`.Template`),
groups (:class:`.Group`) and users (:class:`.User`), for example:

.. code-block:: python3

    # Retrieve an existing collection by its identifier.
    collection = manager.collection(identifier="my_collection")

    # Create a new template with the given metadata. If the template already exists, it
    # will be retrieved by its identifier.
    template = manager.template(
        identifier="my_template",
        title="My template",
        create=True,
    )

Common methods, namely for searching (:class:`.Search`) as well as miscellaneous
functionality (:class:`.Miscellaneous`), are offered directly by corresponding helper
classes:

.. code-block:: python3

    # Perform a search for records.
    response = manager.search.search_resources("record")

    # Get all deleted resources.
    response = manager.misc.get_deleted_resources()

.. tip::
    It is also possible to send individual requests directly using
    :meth:`.KadiManager.make_request`, while still ensuring a common handling of things
    like authorization and errors. This method is especially useful for endpoints for
    which no dedicated functionality exists yet, depending on the versions of kadi-apy
    and Kadi4Mat itself that are used.

Common conversion functionality is offered via the :mod:`conversion
<kadi_apy.lib.conversion>` module. The following shows an example on how to use it:

.. code-block:: python3

    from kadi_apy import conversion

    with open("/path/to/file.json", mode="rb") as f:
        json_data = json.load(f)

    # Convert plain JSON to a Kadi4Mat-compatible extra metadata structure.
    extras = conversion.json_to_kadi(json_data)

KadiManager
-----------

.. autoclass:: kadi_apy.lib.core.KadiManager
    :members:

Record
------

.. autoclass:: kadi_apy.lib.resources.records.Record
    :members:
    :inherited-members:

Collection
----------

.. autoclass:: kadi_apy.lib.resources.collections.Collection
    :members:
    :inherited-members:

Template
--------

.. autoclass:: kadi_apy.lib.resources.templates.Template
    :members:
    :inherited-members:

Group
-----

.. autoclass:: kadi_apy.lib.resources.groups.Group
    :members:
    :inherited-members:

User
----

.. autoclass:: kadi_apy.lib.resources.users.User
    :members:
    :inherited-members:

Miscellaneous
-------------

.. autoclass:: kadi_apy.lib.misc.Miscellaneous
    :members:
    :inherited-members:

Search
------

.. autoclass:: kadi_apy.lib.search.Search
    :members:
    :inherited-members:

Exceptions
----------

.. automodule:: kadi_apy.lib.exceptions
    :members:

Conversion
----------

.. automodule:: kadi_apy.lib.conversion
    :members:
